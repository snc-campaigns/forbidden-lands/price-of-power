---
Players:
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Galivantis]]"
  - "[[Jyrman Lightbreeze]]"
  - "[[Petrine the Nimble]]"
Location(s):
  - "[[Dankwood]]"
Session(s):
  - "[[Session 1]]"
  - "[[Session 2]]"
Tile Reference(s):
  - "[[Cabin in the Woods|W10]]"
---
Quarter 1
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Lead the way | Not recorded | Not recorded | Move to hex W10 |
| Bryndar | Yes | Scout | Not recorded | Not recorded | Discovers a [[Cabin in the Woods\|Cabin]] |
| Galivantis | No | N/A | N/A | N/A | N/A |
| Jyrman | No | N/A | N/A | N/A | N/A |
| Petrine | No | N/A | N/A | N/A | N/A |
Bryndar discovers a cabin that's overgrown. As he approaches the cabin, he finds the door amidst the overgrowth.
Quarter 2
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Rest | N/A | N/A | N/A |
| Bryndar | Yes | Explore cabin | Not recorded | Not recorded | See detail |
| Galivantis | Yes | Rest | N/A | N/A | N/A |
| Jyrman | Yes | Repair item | Not recorded | Not recorded | Tent permanently damaged |
| Petrine | Yes | Keep watch | Not recorded | Not recorded | [[Troll\|A troll]] appears, see detail below |
It looks to Bryndar that nobody has been [[Cabin in the Woods|here]] since the descent of the blood mist. The furniture lies in ruins, the cabin is dusty, and a pair of skeletons can be found in corner. One of the skeletons is adorned with a silver medallion. Bryndar retrieves the silver medallion. There is a [[Wyrm]] symbol on it. In a nearby footlocker, he discovers a buckle from a shield, a rusted sword, and rusted chainmail.

As Petrine is keeping watch, she notices a whimpering puppy approaching the cabin, seemingly wounded deeply by [[Troll|some unidentified wild animal]]. Petrine tries to beckon the puppy over, but the puppy appears to counter this by beckoning Petrine away from the cabin.

Deciding that ignoring the puppy is not a wise option, Petrine wakes the others so that they can prepare for whatever is to come. Before long, the trees seem to break in the distance, and loud booming footfalls can be heard. [[Troll|A troll]] approaches the cabin. [[Troll combat - Day 5|Combat begins]], see linked file for a detailed combat report.
Quarter 3
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Sleep | N/A | N/A | N/A |
| Bryndar | Yes | Rest | N/A | N/A | N/A |
| Jyrman | Yes | Rest | N/A | N/A | N/A |
| Petrine | Yes | Keep watch | Not recorded | Not recorded | Not recorded |
Quarter 4
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Butcher corpse | 3 | 0 | 3 units troll meat |
| Bryndar | Yes | Sleep | N/A | N/A | N/A |
| Jyrman | Yes | Keep watch | Not recorded | Not recorded | Not recorded |
| Petrine | Yes | Sleep | N/A | N/A | N/A |
Aria notices the smell of the troll corpse getting worse as time passes. Not wanting the meat to go to waste, Aria harvests the troll for meat. This turns out to be [[Troll Rot|a bad idea]].