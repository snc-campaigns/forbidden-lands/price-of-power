---
Players:
  - "[[Jyrman Lightbreeze]]"
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Galivantis]]"
  - "[[Petrine the Nimble]]"
Session(s):
  - "[[Session 1]]"
Location(s): 
tags:
  - NotableEvent
Tile Reference(s):
  - 
---
In the first session, the characters find themselves imprisoned by the Rust Brothers one by one. By some fortune, the party end up in the same cage, forming the basis for the party.

Once the party have all been captured, the fanatic branch of worshippers of the Raven - [[Reapenters]] - ambush [[Rust Brothers]], easily slaughtering their foes... The party seems to have never met with such Raven worshippers and are unaware that they should have immediately left. The Blackwings begin rounding up the humans at which point Bryndar tells  
the party they should do their best to run away.

The party escapes to the wilderness and attempts to identify where they've ended up, though they initially have little luck in this venture. Owing to their lack of preparation, the party ends up cold and tired after spending the night outdoors with no shelter.  

After a cold night and half a day, the party realises they're in the Dankwood, a hostile land that is far from the civilisation each is used to.