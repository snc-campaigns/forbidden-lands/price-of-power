---
Players:
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Jyrman Lightbreeze]]"
  - "[[Petrine the Nimble]]"
Location(s):
  - "[[Dankwood]]"
Session(s):
  - "[[Session 2]]"
  - "[[Session 3]]"
Tile Reference(s):
  - [[X09]]
---
Activities:
- Q1:
	- Aria fills up on ammo
	- Bryndar forages for shrooms, 1 unit
	- Jyrman keeps watch
	- Petrine forages for shrooms, 1 unit
- Q2:
	- Aria cooks 3 units shrooms
	- Bryndar tries to remember about the tower in the distance, remembers nothing
	- Jyrman 
	- Petrine keeps watch
- Q3:
	- Aria
	- Bryndar
	- Jyrman
	- Petrine
- Q4:
	- Aria
	- Bryndar
	- Jyrman
	- Petrine