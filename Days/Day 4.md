---
Players:
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Galivantis]]"
  - "[[Jyrman Lightbreeze]]"
  - "[[Petrine the Nimble]]"
Location(s):
  - "[[Dankwood]]"
Session(s):
  - "[[Session 1]]"
Tile Reference(s):
  - 
---
Quarter 1
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Sleep | N/A | N/A | N/A |
| Bryndar | Yes | Forage | 1 | 0 | 1 unit of food |
| Galivantis | No | N/A | N/A | N/A | N/A |
| Jyrman | Yes | Forage | 1 | 0 | 1 unit food |
| Petrine | Yes | Forage | 0 | 1 | Failure, attacked by boar |

Quarter 2
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Lead the way | Not recorded | Not recorded | Not recorded |
| Bryndar | Yes | Scout | Not Recorded | Not recorded | Not recorded |
| Galivantis | No | N/A | N/A | N/A | N/A |
| Jyrman | No | N/A | N/A | N/A | N/A |
| Petrine | No | N/A | N/A | N/A | N/A |
Quarter 3
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Make camp | Not recorded | Not recorded | Not recorded |
| Bryndar | Yes | Sleep | N/A | N/A | N/A |
| Galivantis | Yes | Forage | 3 | 0 | 3 unit food |
| Jyrman | No | N/A | N/A | N/A | N/A |
| Petrine | Yes | Forage (Fish) | 0 | 1 | Fishing rod damaged |
Quarter 4
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Repair item | Not recorded | Not recorded | Tent repaired |
| Bryndar | No | N/A | N/A | N/A | N/A |
| Galivantis | Yes | Keeps watch | Not recorded | Not recorded | Fail? |
| Jyrman | Yes | Repair item | Not recorded | Not recorded | Fishing rod repaired |
| Petrine | No | N/A | N/A | N/A | N/A |
