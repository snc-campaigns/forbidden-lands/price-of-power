---
Players:
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Galivantis]]"
  - "[[Jyrman Lightbreeze]]"
  - "[[Petrine the Nimble]]"
Location(s):
  - "[[Dankwood]]"
Session(s):
  - "[[Session 1]]"
Tile Reference(s):
  - 
---
Activites:
- Aria takes down camp  
- Aria leads the way  
- Bryndar scouts, discovers the half-rotten body of an unfortunate adventurer with a bronze medallion from the rust brothers.   
	- Decides to desecrate the body by dismembering it and hanging it up to feed the ravens.
	- This turns out to be a [[Corpse Rot|bad idea]]
- Party resupplies water
- Petrine fishes in the body of water, catches enough for 2 people to eat  
- Bryndar also catches enough food for 2 people to eat  
- Aria sets camp, tent takes damage and is considered broken  
- Bryndar keeps watch. All sounds die down, and then his hair stands on end. The Blood mist rolls in.  
- After the events of the night, Bryndar attempts to raise morale by singing a hymn. Aria recovers 1 Empathy.  
- Bryndar, caught [[Corpse Rot]], has to roll Endurance. Fails, pushes, uses Pride.
  
Party discloses to one another that they all accepted the deal. As the blood mist rolls in at night, Aria, Galivantis, and Petrine fail to escape to the treetops and have to resist the effects of the blood mist - Jyrman uses pride to succeed,   

Galivantis uses pride to resist effects of blood mist but fails. Loses Pride. All but Aria fail.  

After their initial failure, Galivantis and Petrine get up the tree. Aria fails once more. Takes more Empathy damage.

Aria then gets assisted up the tree, finally making it out of the mist.

 [[Session 1]]  #Dankwood 