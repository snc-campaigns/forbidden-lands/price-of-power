---
Players:
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Jyrman Lightbreeze]]"
  - "[[Petrine the Nimble]]"
Location(s):
  - "[[Dankwood]]"
Session(s):
  - "[[Session 2]]"
Tile Reference(s):
  - "[[Cabin in the Woods|W10]]"
---
Quarter 1
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Cook | Not recorded | Not recorded | 1 die food supply handed to Aria, Bryndar, and Jyrman each. |
| Bryndar | Yes | Lore | Not recorded | Not recorded | See [[Troll]] for details uncovered by this check |
| Jyrman | Yes | Scout | Not recorded | Not recorded | Failed to scout |
| Petrine | Yes | Forage | 1 | Not recorded | 1 units food |
Notes: Aria is unwell after virulence result. She has [[Troll Rot]]. Jyrman heals her up.
Quarter 2
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Scout | 2 | Not recorded | Certain the rock we're looking for is not in X09 |
| Bryndar | Yes | Scout (assist) | N/A | N/A | N/A |
| Jyrman | Yes | Rest | N/A | N/A | N/A |
| Petrine | Yes | Forages | 0 | 0 | 0 units foods |
Quarter 3
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Sleep | N/A | N/A | N/A |
| Bryndar | Yes | Tells a Story | 2 | 0 | Tells a story about the [[Raven]] |
| Jyrman | Yes | Keep watch | Not recorded | Not recorded | No event |
| Petrine | Yes | Sleep | N/A | N/A | N/A |

Wolves howl in the distance. During watch, Jyrman and Bryndar hear something approaching. A majestic white elk with antlers spreading wide enters the glade. It stops and stares at Jyrman and Bryndar.

Jyrman attempts to remember something relating to the beast, but remembers nothing. He knows he's heard of or read about it before though.

Jyrman speaks to it in Elvish, the Elk seems to understand, bows its head slightly. He apologises for disturbing the Elk, explains their purpose.

The elk leaves peacefully, the trees seeming to bend to get out of his way.
Jyrman attempts to remember about the Elk. 2 successes:
- Massive white elks known as the Guardians of the Forest
- Ensure a safe journey to those who show them respect
- Valuable target for hunters as their pelt is valuable

Quarter 4
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Assists with watch | Not recorded | Not recorded | No event |
| Bryndar | Yes | Sleep | N/A | N/a | N/A |
| Jyrman | Yes | Sleep | N/A | N/A | N/A |
| Petrine | Yes | Keep watch | Not recorded | Not recorded | No event |
