---
Players:
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Galivantis]]"
  - "[[Jyrman Lightbreeze]]"
  - "[[Petrine the Nimble]]"
Location(s):
  - "[[Dankwood]]"
Session(s):
  - "[[Session 1]]"
Tile Reference(s):
  - 
---
Activities:
- Jyrman sets a decent camp  
- Galivantis remembers an ominous poem that relates to Dankwood
- Aria attempts to hunt and discovers a mouse, though it escapes  
- Bryndar discovers a path to  the body of water
Notable changes:
- Jyrman's tent takes 1 die of damage  
- Bryndar runs out of water

#NotableEvent
In the middle of the night, the party has a collective dream of their homes. [[Greed|A man wearing a crow mask and a feathered robe]] appears in the dream. Jyrman feels as if it could be himself, but more powerful and secretive. [[Greed|The man]] explains he is [[Greed]] and proposes that Jyrman visit a [[Sorcerer's Tower|sorcerer's tower]] with a sorcerer.

He apologises for the rude way of reaching out and offers any of the following:  
- Silver
- Information
- Keep everything in the tower except the [[Heart of the Sorcerer|heart of the sorcerer]] that owned the [[Sorcerer's Tower|tower]]
  
Following resolutions:
- Jyrman accepts the offer
- Galivantis makes a counteroffer that they want supplies but accepts
- Aria accepts the offer
- Bryndar accepts the offer
- Petrine accepts the offer
  
Information disclosed:
- A few hours journey to the north, a red snail shaped rock. Dig 5 feet down for your payment [[Goals|]]
- The sorcerer built tools of silver, a knife, a bird, and a ring
- [[Sorcerer's Tower|The tower]] is a few days journey to the north


 [[Session 1]]  #Dankwood 