---
Players:
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Jyrman Lightbreeze]]"
  - "[[Petrine the Nimble]]"
Location(s):
  - "[[Dankwood]]"
Session(s):
  - "[[Session 2]]"
Tile Reference(s):
---
Quarter 1
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Hunt | 3 | 0 | 4 units meat, 2 pelts (abandoned) |
| Bryndar | Yes | Assists hunting | N/A | N/A | N/A |
| Jyrman | Yes | Assists hunting | N/A | N/A | N/A |
| Petrine | Yes | Assists hunting | N/A | N/A | N/A |

Quarter 2
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Lead the way | 2 | 0 | N/A |
| Bryndar | Yes | Scout | 1 | 0 | See detail below |
| Jyrman | No | N/A | N/A | N/A | N/A |
| Petrine | No | N/A | N/A | N/A | N/A |
As Bryndar is scouting, he notices a troupe of Jesters in the forest. This seems weird, and he advises Aria to avoid them. As the Jesters pass, the party notice a red snail shaped rock that [[Greed]] mentioned in their dream.
Quarter 3
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Cook | Not recorded | Not recorded | 4 units of cooked meat. Handed out amongst needy in the party. |
| Bryndar | Yes | Assists Petrine | N/A | N/A | N/A |
| Jyrman | Yes | Assists Petrine | N/A | N/A | N/A |
| Petrine | Yes | Digs at snail shaped rock | N/A | N/A | See details below |
Discovers a stone chest, inside a blanket we find 13 silver coins with a iridescent blue eye on them. Eye reminds Bryndar of the Ballad of the Lonely Lady. A story of a lady who decided to never know shame or fear again. These emotions became monsters which hunted her in her own home and burned the home down.
Quarter 4
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Sleep | N/A | N/A | N/A |
| Bryndar | Yes | Sleep | N/A | N/A | N/A |
| Jyrman | Yes | Make camp | 1 | 0 |  |
| Petrine | Yes | Keep watch | Not recorded | Not recorded | No event |

