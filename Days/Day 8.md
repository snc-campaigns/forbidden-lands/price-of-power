---
Players:
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Jyrman Lightbreeze]]"
  - "[[Petrine the Nimble]]"
Location(s):
  - "[[Dankwood]]"
Session(s):
  - "[[Session 2]]"
Tile Reference(s):
  - "[[X09]]"
---
Quarter 1
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Scout | 1 | 1 | Abundant fungi, 1 wound to agi |
| Bryndar | Yes | Keep watch | Not recorded | Not recorded | No event |
| Jyrman | Yes | Sleep | N/A | N/A | N/A |
| Petrine | Yes | Sleep | N/A | N/A | N/A |

Quarter 2
---

A song echoes around the camp. We find ourselves eye to eye with a fox. Fox silently trots forward towards Jyrman, who has begun speaking to it in Elvish. The fox stops about a foot away from Jyrman, its faintly glowing eyes focused on Jyrman.

Jyrman offers some food and water to the fox. It stares at the ground between Jyrman and itself. Jyrman takes this as an indication of wanting to dig, and puts his hand out towards the ground. The fox approaches and puts its paw on Jyrman's hand, the fox grins at Jyrman and opens its mouth. The mouth widens, and widens, revealing rows upon rows of teeth.

It looks like Jyrman is the snacc. Fear attack. Everyone takes 2 wits damage. The fox disappears in a flash.

Bryndar isn't quite sure of what this was, however makes a connection to this probably being a [[Demon]]. He's fairly sure that Jyrman has been marked.

Quarter 3
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Forage water | 0 | 0 | Auto success with willpower |
| Bryndar | Yes | Keep watch | Not recorded | Not recorded | No event |
| Jyrman | Yes | Sleep | N/A | N/A | Missed that he had rested 2 quarters ago (oops) |
| Petrine | Yes | Forage food | 1 | 0 | 1 unit fungi |

Quarter 4
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria | Yes | Sleep | N/A | N/A | N/A |
| Bryndar | Yes | Sleep | N/A | N/A | N/A |
| Jyrman | Yes | Keep watch | N/A | N/A | N/A |
| Petrine | Yes | Sleep | N/A | N/A | N/A |
