---
Day:
  - "[[Day 5]]"
Allies:
  - "[[Jyrman Lightbreeze|Jyrman]]"
  - "[[Aria Brightmoon|Aria]]"
  - "[[Petrine the Nimble|Petrine]]"
  - "[[Bryndar]]"
Enemies:
  - "[[Troll]]"
tags: 
Location(s):
  - "[[Cabin in the Woods]]"
---
Rounds:
- Round 1: 
	- Jyrman stays in house
	- Aria also stays in the house, but attacks using a sling, 2 successes
		- Sling deals 1 damage per success
		- Troll blocks 1 damage
		- Total output of 1 damage
	- Troll reveals its name is Gormold, throws Petrine into Bryndar, neither dodge.
		- 1 damage per success
		- Both attempt to dodge
			- 1 damage dodged by Petrine
			- 0 damage dodged by Bryndar
				- Pushes, 1 damage to agility
		- Bryndar takes full 2 damage, Petrine takes 1 damage to strength
	- Petrine stands up and slashes, 3 successes
		- 1 damage per success
		- Troll does not mitigate or damage its armour
		- Total 3 damage
	- Bryndar stands up and slashes, 2 successes
		- 1 damage per success
		- Troll does not mitigate damage
		- Troll takes 2 damage to armour
		- Total 2 damage
- Round 2
	- Jyrman moves in and attacks with staff, no successes
	- Aria reloads and attacks with sling, 1 success, troll takes 1 damage
	- Troll:
		- throws Jyrman away, 3 damage, 1 overkill, crit wound is "Groin Hit", d6 days healing time (heals [[Day 10]] Q2)
		- Bryndar is incapacitated by stench, has to crawl away
	- Petrine deals 3 damage, troll blocks 0 damage
	- Bryndar crawls away
- Round 3:
	- Jyrman crawls away
	- Aria deals 4 damage , troll blocks 1
	- Troll collapses
	- 