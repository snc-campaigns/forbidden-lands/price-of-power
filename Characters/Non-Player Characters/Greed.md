---
aliases:
  - Mysterious sorcerer
Race: Unknown
Association(s):
  - Unknown
Appearances:
  - "[[Day 2]]"
Session(s):
  - "[[Session 1]]"
tags:
  - NonPlayerCharacter
---
A sorcerer that appeared 