---
Tile Effect: Abundant hunting ground
Connects To:
  - "[[X09]]"
Region: "[[Dankwood]]"
aliases:
  - W10
tags:
  - Location
---
An abandoned cabin, overgrown with ivy, lies amidst a bountiful hunting ground. The door remains in place, seemingly providing decent shelter with little effort.