```leaflet
id: worldmap
image: 
  - [[Image.jpg]]
height: 500px
lat: 21
long: 54
minZoom: 5
maxZoom: 10
defaultZoom: 8
unit: meters
scale: 1

mapmarker:
  - Location
  - Fortress
  - Day

markersFolder:
  - "Days"
  - "Locations"
```
