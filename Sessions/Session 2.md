---
Attendees:
  - "[[Jyrman Lightbreeze]]"
  - "[[Aria Brightmoon]]"
  - "[[Bryndar]]"
  - "[[Petrine the Nimble]]"
Session Date: 2024-02-03
Did you show up?: true
Did you explore a new hex?: true
Did you kill a monster?: true
Did you learn a new piece of lore?: true
Did you find at least 1 gp worth of treasure?: true
Did you advance the overall plot?: false
Did you complete an adventurer site?: false
Did you build a new function in your stronghold?: false
Used pride: 
Suffered DS: 
Advanced P Plot: 
Risked life for PC: 
Extraordinary action:
  - "[[Petrine the Nimble]]"
  - "[[Aria Brightmoon]]"
---
