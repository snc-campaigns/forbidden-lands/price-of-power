---
Attendees:
  - "[[Jyrman Lightbreeze]]"
  - "[[Bryndar]]"
  - "[[Petrine the Nimble]]"
  - "[[Aria Brightmoon]]"
  - "[[Galivantis]]"
Session Date: 2024-01-13
Did you show up?: true
Did you explore a new hex?: true
Did you kill a monster?: false
Did you learn a new piece of lore?: false
Did you find at least 1 gp worth of treasure?: false
Did you advance the overall plot?: false
Did you complete an adventurer site?: false
Did you build a new function in your stronghold?: false
Used pride:
  - "[[Bryndar]]"
  - "[[Galivantis]]"
  - "[[Jyrman Lightbreeze]]"
Suffered DS:
  - "[[Bryndar]]"
Advanced P Plot: 
Risked life for PC: 
Extraordinary action:
  - "[[Jyrman Lightbreeze]]"
---
Experience checklist:  
- Attending
	- all
- Travelling at least 1 hex
	- all
- Dark secret
	- Discussed in private with players  
- Pride
	- Bryndar
	- Galivantis
	- Jyrman  
- Found treasure worth at least 1 gold
	- all  
- Extraordinary feat
	- Jyrman