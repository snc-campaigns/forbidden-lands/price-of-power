---
aliases:
  - Blackwings
tags:
  - "#Organisation"
---
Doomsday cult worshipping the [[Raven]] with the goal of eradicating humans from the Ravenland. Half-elves are also welcome, though do not share the same burden as the human cult members, who upon achieving their goal, plan to commit suicide.