---
Players: 
Location(s): 
Session(s): 
Tile Reference(s):
---
Quarter 1
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria |  |  |  |  |  |
| Bryndar |  |  |  |  |  |
| Galivantis |  |  |  |  |  |
| Jyrman |  |  |  |  |  |
| Petrine |  |  |  |  |  |

Quarter 2
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria |  |  |  |  |  |
| Bryndar |  |  |  |  |  |
| Galivantis |  |  |  |  |  |
| Jyrman |  |  |  |  |  |
| Petrine |  |  |  |  |  |

Quarter 3
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria |  |  |  |  |  |
| Bryndar |  |  |  |  |  |
| Galivantis |  |  |  |  |  |
| Jyrman |  |  |  |  |  |
| Petrine |  |  |  |  |  |

Quarter 4
---
| Character | Acted? | Activity | Successes | Failures | Outcome |
| ---- | ---- | ---- | ---- | ---- | ---- |
| Aria |  |  |  |  |  |
| Bryndar |  |  |  |  |  |
| Galivantis |  |  |  |  |  |
| Jyrman |  |  |  |  |  |
| Petrine |  |  |  |  |  |

