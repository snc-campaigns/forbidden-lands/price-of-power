The willpower threshold gives you additional willpower at the end of a session by adding your willpower threshold to your willpower and divide by two.

Willpower Threshold = (Empathy + Sum of Ranks in Kin Talents + Sum of Ranks in Profession Talents) / 2

End of Session:
- Willpower + Willpower Threshold / 2
- Round to nearest number
- This is your new willpower value