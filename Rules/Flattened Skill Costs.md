Skill cost progression is now:
- Rank 1: 5
- Rank 2: 10
- Rank 3: 20
- Rank 4: 35
- Rank 5: 60
- Rank 6 (if GM allows): 100