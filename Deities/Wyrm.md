According to the Congregation of the Serpent, the god allowed a raven to carry him during his journey. The [[raven]] is considered to be a holy, but not divine, bird while the snake was the god. They name him Wyrm.  
  
The leader of the [[Congregation of the Serpent]] is called the Psychopomp. The [[Congregation of the Serpent]] in the Forbidden Lands leader is the Psychopomp [[Aspis]]. The church’s leader has been based in the village of [[Farhaven]] since the city of [[Falender]] was burned to the ground during the wars. [[Aspis]] nurtures a dream of rebuilding [[Falender]] and the serpent temple there. The [[Congregation of the Serpent]] is very important culturally, because they keep a large library in [[Farhaven]].

#Deity 