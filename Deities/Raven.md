According to the [[Raven Church]], it was, of course, the raven that was divine. They call their god Raven. They fight the [[Rust Brothers]]. The [[Raven Church]] also honors the water goddess, [[Flow]], since she is of great significance to life.
  
The [[Reapenters]] are a doomsday cult sprung from the Raven Church. They worship the raven as a carrion eater, and call him [[Corax]]. The [[Reapenters]] believe the wrath of the god was awakened when the humans defied the ancient Shift and entered Ravenland. [[Corax]] will only be appeased once Ravenland is again free of humans.  
  
The [[Reapenters]] believe they have a sacred duty to kill all humans, after which the last [[Reapenters]] will take their own lives. These killers, dressed in black and adorned with feathers, have slaughtered whole villages under the command of their leader, [[Badalar the Butcher]]. The [[Reapenters]] dismember their victims if possible, and enact rituals in which they hang the body parts in trees or on rocks where they can be eaten by the god’s carrion birds. They surround themselves with half-tame ravens and answer “Nevermore!” when their priests list all the sins and horrors committed by humans today.

Stories known:


#Deity 