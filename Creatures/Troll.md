---
Afflictions caused: "[[Troll Rot]]"
tags:
  - Creature
---
Weaknesses:
- Turns to stone in direct sunlight

Known Lore:
- Trolls are born underground.
- They journey to the surface as they grow for space and food
- Trolls eat anything they can
	- Innards and flesh are a favourite
	- They absolutely stink due to eating carrion, refuse, etc.
	- Turn to stone in direct sunlight
